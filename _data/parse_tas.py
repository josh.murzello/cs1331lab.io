import pandas
import yaml

df_ta = pandas.read_csv('ta-info.csv')
df_ta.columns = [header.lower() for header in df_ta.columns]
df_ta = df_ta.fillna('TBD')

tas = []
for index, row in df_ta.iterrows():
    tas.append({
        'name': row['name'],
        'email': row['email'],
        'os': row['os'],
        'section': row['section'],
        'image': 'fall2018/' + row['gt username'].lower() + '.jpg'
        }
    )

tas = sorted(tas, key=lambda ta: ta['section'])

with open('tas.yml', "w") as ta_yaml:
    yaml.dump(tas, ta_yaml)
